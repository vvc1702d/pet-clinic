case $1 in

  install)
    sudo apt-get install -y ca-certificates curl gnupg lsb-release
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update -y
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
    sudo dpkg -i minikube_latest_amd64.deb
    sudo usermod -aG docker $USER && newgrp docker
    minikube kubectl -- get po -A
    alias mkc="minikube kubectl --"
    minikube start --driver='docker'
    mkc config set-context $(mkc config current-context) --namespace=pet-clinic
    ;;

  deploy)
    echo "deploying"
    minikube kubectl -- create namespace pet-clinic
    minikube kubectl -- apply -f mysql/configmap.yaml
    minikube kubectl -- apply -f mysql/secret.yaml
    minikube kubectl -- apply -f mysql/service.yaml
    minikube kubectl -- apply -f mysql/storage.yaml
    minikube kubectl -- apply -f mysql/deployment.yaml
    minikube kubectl -- apply -f app/configmap.yaml
    minikube kubectl -- apply -f app/service.yaml
    minikube kubectl -- apply -f app/deployment.yaml
    minikube kubectl -- get deployments
    ;;

  undeploy)
    echo "deleting all app and mysql resources"
    minikube kubectl -- delete -f app/configmap.yaml
    minikube kubectl -- delete -f app/service.yaml
    minikube kubectl -- delete -f app/deployment.yaml
    minikube kubectl -- delete -f mysql/configmap.yaml
    minikube kubectl -- delete -f mysql/secret.yaml
    minikube kubectl -- delete -f mysql/deployment.yaml
    minikube kubectl -- delete -f mysql/service.yaml
    minikube kubectl -- delete -f mysql/storage.yaml
    minikube kubectl -- delete namespace pet-clinic
    minikube kubectl -- get deployments
    ;;

  rebuild)
    echo "deleting all app and mysql resources"
    minikube kubectl -- delete -f app/configmap.yaml
    minikube kubectl -- delete -f app/service.yaml
    minikube kubectl -- delete -f app/deployment.yaml
    minikube kubectl -- delete -f mysql/configmap.yaml
    minikube kubectl -- delete -f mysql/secret.yaml
    minikube kubectl -- delete -f mysql/deployment.yaml
    minikube kubectl -- delete -f mysql/service.yaml
    minikube kubectl -- delete -f mysql/storage.yaml
    minikube kubectl -- delete namespace pet-clinic
    minikube kubectl -- get deployments

    minikube kubectl -- create namespace pet-clinic
    minikube kubectl -- apply -f mysql/configmap.yaml
    minikube kubectl -- apply -f mysql/secret.yaml
    minikube kubectl -- apply -f mysql/service.yaml
    minikube kubectl -- apply -f mysql/storage.yaml
    minikube kubectl -- apply -f mysql/deployment.yaml
    minikube kubectl -- apply -f app/configmap.yaml
    minikube kubectl -- apply -f app/service.yaml
    minikube kubectl -- apply -f app/deployment.yaml
    minikube kubectl -- get deployments
    ;;

  rollout)
    echo "Performing rollout"
    minikube kubectl -- rollout restart deployment mysql
    minikube kubectl -- rollout restart deployment pet-clinic
    minikube kubectl -- rollout status deployment mysql
    minikube kubectl -- rollout status deployment pet-clinic
    minikube kubectl -- rollout history deployment mysql
    minikube kubectl -- rollout history deployment pet-clinic
    ;;

  stop)
    echo "stopping minikube. Your cluster will be saved"
    minikube stop
    ;;

  start)
    echo "starting minikube. Pods in cluster will be resumed"
    minikube start --driver='docker'
    ;;

  app)
	  mkip=$(minikube ip)
	  echo "http://$mkip:30080"
    ;;

  mysql)
	  mkip=$(minikube ip)
    echo "using petclinic user"
	  mysql -h $mkip -P 30306 -u petclinic -p
    ;;

  help)
    echo "SCRIPT REQUIRES TO BE IN THE REPO'S ROOT DIRECTORY"
    echo "./mkc.sh install - install minikube, docker and start minikube (works only with ubuntu)"
    echo "./mkc.sh deploy - deploy all app components and namespace \"pet-clinic\". This can be also used to update objects though it's recommended to make a rollout"
    echo "./mkc.sh undeploy - delete all app components and namespace (this will also remove volume make sure you made a backup)"
    echo "./mkc.sh rebuild - flush all components and set them up again (use rollout for updates)"
    echo "./mkc.sh rollout - makes a rollout for both mysql and app deployment"
    echo "./mkc.sh app - print application URL with default port"
    echo "./mkc.sh mysql - Connect to MySQL DB"
    ;;

  *)
    echo "no arguments\nprovide: install, deploy or undeploy\nexiting program"
    ;;
esac
