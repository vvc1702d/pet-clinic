# Preface

This documentation will guide you through the process of installing (only Ubuntu), starting, deploying, managing and undeploying your local kubernetes cluster.

Many things will be wrapped in shell script to make use of this cluster easier. Script can be found in root of a cluster repo and from that point it must be executed

  

Cluster consists of two deployments broken down to two directories- `mysql` and `app`. Those folders contains deployment, configmap, service, secret and storage definitions.

Configuration is already fully functional and ready to deploy. If you require further configuration see section `3. Configuration adjustments`

# 1. Installation, deployment and use

## Installation
### Ubuntu
For Ubuntu installation is automated using script. To Install it simply open terminal got to director where script is located and run `./mkc install`

After installation you should be able to run `minikube help` in terminal

### MacOS
Follow these instructions:
	Minikube: https://minikube.sigs.k8s.io/docs/start/
	Docker: https://docs.docker.com/desktop/mac/install/
After installation you should be able to run `minikube help` in terminal

Start minikube using `minikube start --driver='docker'`


## All after successful start
In your shell add an alias:
`alias mkc="minikube kubectl --"`

Set default namespace to `pet-clinic`:
```mkc config set-context $(kubectl config current-context) --namespace=pet-clinic```


## Deployment and management using `mkc.sh` script

### `./mkc.sh help`
This command will return all arguments and short information what each of them do

### `./mkc.sh deploy`
Deploy all pet-clinic and mysql objects. If ran when objects already exist it will update their configuration (if there was a change). This is recommended way to update changed manifests to avoid forgotten unapplied manifests that later might ruin whole deployment

### `./mkc.sh undeploy`
Delete all app components and namespace (this will also remove volume make sure you made a backup)

### `./mkc.sh rebuild`
Flush all components and create them again. It's simply deploy and undeploy in one command

### `./mkc.sh rollout`
Makes a rollout for both mysql and app deployment

### `./mkc.sh install`
Install minikube, docker and start minikube (works only with ubuntu)

### `./mkc.sh start`
Start minikube. All obejcts defined before `minikube stop` will be resumed.

### `./mkc.sh stop`
Stop minikube. This won't remove any objects. All objects will be resumed after start

### `./mkc.sh app`
Print application URL with default port

### `./mkc.sh mysql`
Connect to MySQL DB

## Management Using `mkc`
### list all resources in namespace
`mkc get all`

### list available pods
`mkc get pods`

### print pod's logs
`mkc logs pods <pods_id> -f`

### print detailed information about pod
`mkc describe pods <pods_ip>`

### list deployments
`mkc get deployments`

### update and apply manifest
`mkc apply -f <path/to/manifest.yaml>`

## Use
If you have installed minikube and docker:
	Run `./mkc.sh deploy`
	Run `./mkc.sh app` to get petclinic app URL
	Paste output in your browser

# 2. Structure

## app
Pet-clinic app is defined in `app` directory and requires 3 files:
	- app/deployment.yaml
			Creates Deployment object and is responsible for deploying and configuring pods
	- app/service.yaml
			Creates Service which allows you to connect to it using combination of minikube's ip and port defined in `spec.ports[].nodePort`
	- app/configmap.yaml
			Creates ConfigMap with configuration for pet-clinic app. Here you'll be able to define new `key: value` for further configuration

## mysql
External database which in this case is MySQL is defined in `mysql` directory. This directory consists of:
	- mysql/configmap.yaml
			This file defines two ConfigMaps. First- `mysql-initscript-cm` defines initial script for mysql to run during initialization. You can add SQL there should you require additional data or configuration. Second one `mysql-petclinic-misc` is for miscellaneous data like database name or additional values for image's environment variables
	- mysql/deployment.yaml
			Creates Deployment object, sets environment variables and is responsible for deploying and configuring pods
	- mysql/secret.yaml
			Here are defined secrets. Works just like configMap, but values must be encoded in base64. This Secret definition stores `username` and `password` for additional mysql user 
	- mysql/service.yaml
			Service definition, set to static port for NodePort
	- mysql/storage.yaml
			Storage definition for database. This can be modified if you require more space for DB

# 3. Configuration adjustment

This set is ready to deploy, but if you require changes here's list what can be modified:

## 1. Storage space
If you need more space for DB change value of `storage` in both `pv-mysql` and `mysql-claim`.
THIS OPERATION IS NOT DYNAMIC. YOU'LL MUST FIRST DELETE THESE OBJECTS TO CHANGE IT

`minikube kubectl -- delete -f mysql/storage.yaml`

`mysql/storage.yaml`
```
apiVersion: v1

kind: PersistentVolume

metadata:

  name: pv-mysql

spec:

  accessModes:

    - ReadWriteOnce

  capacity:

    storage: 5Gi << change this

  hostPath:

    path: /data/pv-mysql/

---

apiVersion: v1

kind: PersistentVolumeClaim

metadata:

  name: mysql-claim

spec:

  accessModes:

    - ReadWriteOnce

  resources:

    requests:

      storage: 2Gi << this value must be lower or equal to pv-mysql storage capacity. This is actual storage definition
```

After change apply new objects:
`minikube kubectl -- apply -f mysql/storage.yaml`

## 2. Apply SQL schema
ConfigMap `mysql-initscript-cm` defined in `mysql/configmap.yaml` allows you to insert sql file to be read during init

`mysql/configmap.yaml`
```

apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-initscript-cm
data:
  initdb.sql: |
    CREATE DATABASE IF NOT EXISTS petclinic;
    
    #INSERT YOUR SQL HERE 

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-petclinic-misc
data:
  database: petclinic

```

After save apply new configuration `minikube kubectl -- apply -f mysql/configmap.yaml` and make a rollout `./mkc.sh rollout`

## 3. Configure MySQL AND Pet-clinic using environment variables
In `mysql/deployment.yaml` and `app/deployment.yaml` files you can modify pods using stated in docker image docs flags

#### Example definition for flag with key defined in Secret object
```
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              key: username
              name: mysql-petclinic-credentials
```


#### Example definition for flag with key defined in ConfigMap.yaml
```
        - name: MYSQL_DATABASE
          valueFrom:
            configMapKeyRef:
              key: database
              name: mysql-petclinic-misc
```

#### Template
```
        - name: <flag_name>
          valueFrom:
            <configMapKeyRef for ConfigMap OR secretKeyRef for Secret>:
              key: <key>
              name: <name of ConfigMap or Secret Object>
```

### Deployment definition with defined env vars
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
spec:
  selector:
    matchLabels:
      app: mysql-petclinic
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: mysql-petclinic
    spec:
      containers:
      - image: mysql:5.6
        name: mysql-petclinic
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              key: password
              name: mysql-petclinic-credentials
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              key: password
              name: mysql-petclinic-credentials
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              key: username
              name: mysql-petclinic-credentials
        - name: MYSQL_DATABASE
          valueFrom:
            configMapKeyRef:
              key: database
              name: mysql-petclinic-misc
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
        - name: mysql-initscript-storage
          mountPath: /docker-entrypoint-initdb.d
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-claim
      - name: mysql-initscript-storage
        configMap:
          name: mysql-initscript-cm
```

To make it work you have to create the same key and value in Secret or ConfigMap:

`mysql/configmap.yaml`
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: mysql-petclinic-misc
data:
  database: petclinic
  exampleFlag: test
```

After changes you have to apply and rollout new pods:

```
minikube kubectl -- apply -f mysql/secret.yaml
minikube kubectl -- rollout restart deployment mysql
minikube kubectl -- rollout status deployment mysql
```

## 4. Change database source URL and DB engine

### DB URL
To change it modify values in given keys in  `app/configmap.yaml` 
DB URL must be in this format ``jdbc:mysql://<Database_URL>:<port>/<database>``

### DB Engine
To switch to H2 DB Engine remove code below from `app/deployment.yaml`  
```
          - name: SPRING_PROFILES_ACTIVE
            valueFrom:
              configMapKeyRef:
                key: spring_profiles_active
                name: app-configuration-cm
```

## 5. New docker image
If you have new docker image to deploy  change `spec.template.spec.containers[].image` in `app/deployment.yaml`.
After that apply new config `minikube kubectl -- apply -f app/deployment.yaml`. 
Deployment change is recognized and new pod should be created, but if that won't happen run: `minikube kubectl -- rollout restart deployment pet-clinic && minikube kubectl -- rollout status deployment pet-clinic`
